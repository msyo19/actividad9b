package com.example.msyo19.activad9re2;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private IntentFilter filtro;
    private MyReceiver myReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
         filtro = new IntentFilter();
         filtro.addAction("com.example.msyo19.avtividad9em");
         myReceiver = new MyReceiver();
         registerReceiver(myReceiver,filtro);

    }


    public class MyReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            Toast.makeText(context,"Se recibio la trasnmicion",Toast.LENGTH_SHORT).show();
            Log.d("hola", "onReceive: HOla");
        }
    }



}
